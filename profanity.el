;;; profanity.el --- run Profanity in an Emacs (v)term buffer  -*- lexical-binding: t -*-

;; Copyright (C) 2022 by Martin <debacle@debian.org>
;; Author: Martin <debacle@debian.org>
;; URL: https://salsa.debian.org/debacle/profanity.el
;; Created: 2022-02-12
;; Version: 0.2
;; Keywords: jabber profanity xmpp term vterm

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides the function `profanity` to easily start
;; profanity inside of Emacs.

;; To use the `/editor' command in Profanity, make sure, your
;; ~/.config/profanity/profrc contains:

;; [ui]
;; compose.editor=/usr/bin/emacsclient

;; Note, that after editing a message, Profanity does not show it —
;; you need to press enter in Profanity, and the message will be sent.

;; If Profanity runs in term char mode instead of vterm, all `C-x' key
;; sequences are mapped to `C-c'. E.g. use `C-c b' instead of `C-x b'
;; to switch a buffer.

;;; Code:

(require 'subr-x)
(require 'term)
(setq profanity-use-vterm (require 'vterm-module nil t))
(if profanity-use-vterm (require 'vterm))

(setq profanity-accounts-file "~/.local/share/profanity/accounts")

(defun profanity-accounts ()
  "Returns profanity account names defined in `profanity-accounts-file'."
  (let*
      ((content (with-temp-buffer
		  (insert-file-contents-literally profanity-accounts-file)
		  (keep-lines "^\s*\\[.+\\]\s*$" (point-min) (point-max))
		  (buffer-substring-no-properties (point-min) (point-max)))))
    (mapcar (lambda (s) (string-trim s "\\[" "\\]")) (split-string content))))

(setq profanity-editor-filename 
      "\\.local/share/profanity/editor/.*/compose\\.md$")

(setq profanity-editor-size 4)

(defun profanity-editor-hook ()
  "Edit in neighbouring window,
not higher than \\[profanity-editor-size] lines."
  (if (string-match-p profanity-editor-filename (buffer-file-name))
      (progn
	(make-local-variable 'kill-buffer-hook)
	(add-hook 'kill-buffer-hook 'delete-window nil t)
	(split-window-vertically)
	(if (> (window-body-height) profanity-editor-size)
	    (shrink-window (- (window-body-height) profanity-editor-size))))))

(defun profanity-on-exit (&optional vterm-buffer event)
  (kill-buffer)
  (unless (seq-filter (lambda (buf)
			(string-prefix-p "*profanity" buf))
		      (mapcar #'buffer-name (buffer-list)))
    (remove-hook 'find-file-hook 'profanity-editor-hook)))

;; from https://stackoverflow.com/questions/12624815/
;; how-to-automatically-kill-buffer-on-terminal-process-exit-in-emacs
(unless profanity-use-vterm
  (defadvice term-handle-exit (after term-kill-buffer-on-exit activate)
    (profanity-on-exit)))

(defun profanity-make-buffer-name (profanity-account)
  (concat "profanity" (if (string= "" profanity-account)
			  ""
			(concat " " profanity-account))))

(defun profanity-term (profanity-account)
  "Run profanity in a \\[term] buffer."
  ;; from https://emacs.stackexchange.com/questions/18672/
  ;; how-to-define-a-function-that-calls-a-console-process-using-ansi-term
  (let ((termbuf
	 (apply 'make-term
		(profanity-make-buffer-name profanity-account)
		"/usr/bin/profanity" nil
		(if (member profanity-account (profanity-accounts))
		    (list "--account" profanity-account)))))
    (set-buffer termbuf)
    (term-mode)
    (term-char-mode)
    (switch-to-buffer termbuf)))

(defun profanity-vterm (profanity-account)
  "Run profanity in a \\[vterm] buffer."
  (let ((profanity-buffer-name
	 (concat "*" (profanity-make-buffer-name profanity-account) "*")))
    (if (get-buffer profanity-buffer-name)
	(switch-to-buffer profanity-buffer-name)
      (with-current-buffer (vterm profanity-buffer-name)
	(add-hook 'vterm-exit-functions 'profanity-on-exit nil t)
	(make-local-variable 'vterm-kill-buffer-on-exit)
	(setq vterm-kill-buffer-on-exit nil)
	(use-local-map (copy-keymap vterm-mode-map))
	(local-set-key (kbd "<M-left>") #'vterm--self-insert)
	(local-set-key (kbd "<M-right>") #'vterm--self-insert)
	(local-set-key (kbd "<M-prior>") (lambda () (interactive)
					   (vterm-send-key "<escape>")
					   (vterm-send-key "<prior>")))
	(local-set-key (kbd "<M-next>") (lambda () (interactive)
					  (vterm-send-key "<escape>")
					  (vterm-send-key "<next>")))
	;; https://www.reddit.com/r/emacs/comments/ft84xy/
	;; run_shell_command_in_new_vterm/
	(vterm-send-string "exec /usr/bin/profanity ")
	(if (member profanity-account (profanity-accounts))
	    (vterm-send-string (concat "--account " profanity-account)))
	(vterm-send-return)))))

(defun profanity (profanity-account)
  "Run profanity in a \\[term] or \\[vterm] buffer."
  (interactive (list (completing-read "Account: " (profanity-accounts))))
  (add-hook 'find-file-hook 'profanity-editor-hook)
  (if profanity-use-vterm
      (profanity-vterm profanity-account)
    (profanity-term profanity-account)))
